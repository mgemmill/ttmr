from datetime import datetime
from ttmr.time import Date


def test_date_current():
    assert Date.current().replace(microsecond=0) == Date.now().replace(microsecond=0)
    assert isinstance(Date.current(), Date)
    assert Date.current("2020-03-01 10:10:10") == Date(2020, 3, 1, 10, 10, 10)


def test_date_std_weekday():
    for index, day in enumerate(range(1, 8), 1):
        d = Date(2020, 3, day, 9, 48, 15, 23)
        assert d.std_weekday() == index


def test_date_days_from_start_of_week():
    for index, day in enumerate(range(1, 8)):
        d = Date(2020, 3, day, 9, 48, 15, 23)
        print(f"DAY: {d:%A, %b %d, %Y (%H:%M:%S)}")
        days = d.days_from_start_of_week()
        print(f"DAYS: {days}")
        assert days == index


def test_date_days_to_end_of_week():
    for index, day in enumerate(range(1, 8)):
        index = 6 - index
        d = Date(2020, 3, day, 9, 48, 15, 23)
        print(f"DAY: {d:%A, %b %d, %Y (%H:%M:%S)}")
        days = d.days_to_end_of_week()
        print(f"DAYS: {days}")
        assert days == index


def test_date_start_of_day():
    d = Date(2020, 3, 4, 9, 48, 15, 23)
    assert isinstance(d, datetime)
    assert isinstance(d, Date)
    s = d.start_of_day()
    assert s.year == 2020
    assert s.month == 3
    assert s.day == 4
    assert s.hour == 0
    assert s.minute == 0
    assert s.second == 0
    assert s.microsecond == 0


def test_date_start_of_week():
    for day in range(1, 8):
        d = Date(2020, 3, day, 9, 48, 15, 23)
        print(f"FROM: {d:%A, %b %d, %Y (%H:%M:%S)}")
        s = d.start_of_week()
        print(f"BOW: {s:%A, %b %d, %Y (%H:%M:%S)}")
        assert s.year == 2020
        assert s.month == 3
        assert s.day == 1
        assert s.hour == 0
        assert s.minute == 0
        assert s.second == 0
        assert s.microsecond == 0


def test_date_end_of_week():
    for day in range(1, 8):
        d = Date(2020, 3, day, 9, 48, 15, 23)
        print(f"FROM: {d:%A, %b %d, %Y (%H:%M:%S)}")
        s = d.end_of_week()
        print(f"EOW: {s:%A, %b %d, %Y (%H:%M:%S)}")
        assert s.year == 2020
        assert s.month == 3
        assert s.day == 8
        assert s.hour == 0
        assert s.minute == 0
        assert s.second == 0
        assert s.microsecond == 0
