from freezegun import freeze_time
from ttmr.database import Category
from ttmr.database import Entry
from ttmr.database import Project
from ttmr.time import Date


def test_test_database(db):
    categories = Category.get_all(db)
    assert len(categories) == 3

    projects = Project.get_all(db)
    assert len(projects) == 3

    entries = Entry.get_all(db)
    assert len(entries) == 36


def test_entry_get_last(db):
    entry = Entry.get_last(db, Date(year=2020, month=2, day=10, hour=10, minute=20))
    assert entry.start_time.day == 14
    assert entry.start_time.hour == 13
    assert entry.start_time.minute == 0


def test_entry_day_summary(db):

    with freeze_time("2020-02-14 10:10:10"):
        assert Date.current() == Date(2020, 2, 14, 10, 10, 10)
        entries = Entry.day_summary(db)
        assert len(entries) == 1
        print(entries)
        assert entries[0][1].hour == 7
        assert entries[0][2].hour == 13
        assert entries[0][3] == 390


#  def test_entry_weekly_summary(db):
#      from sqlalchemy import func
#      def day_of_week(dow, sun=0, mon=0, tue=0, wed=0, thu=0, fri=0, sat=0):
#          qry = db.query(
#              Category.name,
#              func.count(Entry.id).label("ENTRY_COUNT"),
#              func.min(Entry.start_time).label("START"),
#              func.max(Entry.end_time).label("END"),
#              func.sum(sun).label("SUNDAY"),
#              func.sum(mon).label("MONDAY"),
#              func.sum(tue).label("TUESDAY"),
#              func.sum(wed).label("WEDNESDAY"),
#              func.sum(thu).label("THURSDAY"),
#              func.sum(fri).label("FRIDAY"),
#              func.sum(sat).label("SATURDAY")
#          )
#          qry = qry.filter(func.extract('dow', Entry.start_time) == dow)
#          qry = qry.group_by(Category.name)
#          return qry
#
#      qry = day_of_week(0, sun=Entry.duration)
#      qry = qry.union(day_of_week(1, mon=Entry.duration))
#      qry = qry.union(day_of_week(2, tue=Entry.duration))
#      qry = qry.union(day_of_week(3, wed=Entry.duration))
#      qry = qry.union(day_of_week(4, thu=Entry.duration))
#      qry = qry.union(day_of_week(5, fri=Entry.duration))
#      qry = qry.union(day_of_week(6, sat=Entry.duration))
#
#      qry = qry.from_self(
#          Category.name,
#          func.sum("ENTRY_COUNT"),
#          func.max("START"),
#          func.max("END"),
#          func.max("SUNDAY")
#
#      ).group_by(Category.name)
#
#      print(qry)
#
#      results = qry.all()
#
#      for row in results:
#          print(row)
#
#      #  assert False
