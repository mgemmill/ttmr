from types import SimpleNamespace
from freezegun import freeze_time
from ttmr.commands import new_session
from ttmr.commands import time_entry
from ttmr.database import Category
from ttmr.database import Entry
from ttmr.database import Project
from ttmr.time import Date


class MockUI:
    def __init__(
        self, db, note=None, start_time=None, end_time=None, duration=None,
    ):
        self.db = db
        self.note = note
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration

        self.category = Category(name="Test")
        self.project = Project(type="00", number=0, name="Test")

        db.add(self.project)
        db.add(self.category)
        db.commit()

    def fetch_entry_inputs(self, categories, projects, last_end_time):
        return self.category, self.project, self.note, last_end_time

    def fetch_end_entry_inputs(self, current_timestamp, start_time):
        return self.end_time, self.duration


def test_test_ui_object():
    with new_session("") as db:
        ui = MockUI(db)
        assert ui.start_time is None
        assert ui.end_time is None
        assert ui.duration is None
        assert ui.category.name == "Test"
        assert ui.project.name == "Test"


@freeze_time("2020-02-22 10:10:10")
def test_time_entry_as_end_time_entry_using_default_start_time():
    cfg = SimpleNamespace()
    cfg.db_path = ""
    with new_session("") as db:
        cfg.db = db

        ui = MockUI(db)
        ui.end_time = Date(2020, 2, 22, 10, 11, 0)
        ui.duration = 10

        time_entry(cfg, ui, end_entry=True)

        entries = db.query(Entry).all()
        assert len(entries) == 1

        assert entries[0].note == ""
        assert entries[0].start_time.hour == 7
        assert entries[0].start_time.minute == 0
        assert entries[0].end_time.hour == 10
        assert entries[0].duration == 10


@freeze_time("2020-02-22 10:10:10")
def test_time_entry_as_end_time_entry_using_previous_start_time():
    cfg = SimpleNamespace()
    cfg.db_path = ""
    with new_session("") as db:
        cfg.db = db

        ui = MockUI(db)
        ui.end_time = Date(2020, 2, 22, 10, 11, 0)
        ui.duration = 10

        # create first entry of the day
        time_entry(cfg, ui, end_entry=True)

        ui.note = "second entry"
        ui.end_time = Date(2020, 2, 22, 10, 15, 0)
        ui.duration = 4

        # create second entry of theday
        time_entry(cfg, ui, end_entry=True)

        entries = db.query(Entry).all()
        assert len(entries) == 2

        assert entries[1].note == "second entry"
        assert entries[1].start_time.hour == 10
        assert entries[1].start_time.minute == 11
        assert entries[1].end_time.hour == 10
        assert entries[1].duration == 4


@freeze_time("2020-02-22 10:10:10")
def test_time_entry_as_start_time_entry_using_default_start_time():
    cfg = SimpleNamespace()
    cfg.db_path = ""
    with new_session("") as db:
        cfg.db = db

        ui = MockUI(db)
        ui.note = "new entry"

        time_entry(cfg, ui, end_entry=False)

        entries = db.query(Entry).all()
        assert len(entries) == 1

        assert entries[0].note == "new entry"
        assert entries[0].start_time.hour == 10
        assert entries[0].start_time.minute == 10
        assert entries[0].end_time is None
        assert entries[0].duration == 0
