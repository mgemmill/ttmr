import pytest
from ttmr.commands import new_session
from ttmr.database import Category
from ttmr.database import Entry
from ttmr.database import Project
from ttmr.time import Date
from ttmr.time import timedelta


@pytest.fixture
def db_session():
    with new_session("") as session:
        yield session


@pytest.fixture
def db(db_session):

    cat_prj = Category(name="Projects")
    cat_issue = Category(name="Issues")
    cat_admin = Category(name="Admin")

    db_session.add(cat_prj)
    db_session.add(cat_issue)
    db_session.add(cat_admin)

    db_session.commit()

    prj_1 = Project(type="PR", number=1, name="Project #1")
    prj_2 = Project(type="PR", number=2, name="Project #2")
    prj_3 = Project(type="PR", number=3, name="Project #3")

    db_session.add(prj_1)
    db_session.add(prj_2)
    db_session.add(prj_3)

    db_session.commit()

    standard_day = [
        (cat_admin, prj_1, 30),  #  7:00 am -  7:30 am
        (cat_admin, prj_2, 30),  #  7:30 am -  8:00 am
        (cat_prj, prj_3, 120),   #  8:00 am - 10:00 am
        (cat_issue, prj_1, 60),  # 10:00 am - 11:00 am
        (cat_prj, prj_2, 120),   # 11:00 am -  1:00 pm
        (cat_admin, prj_3, 30),  #  1:00 pm -  1:30 pm
    ]

    for i in range(9, 15):
        _date = Date(year=2020, month=2, day=i)
        start_time = _date.replace(hour=7, minute=0, second=0, microsecond=0)
        for cat, prj, dur in standard_day:
            end_time = start_time + timedelta(minutes=dur)
            entry = Entry(category=cat, project=prj, start_time=start_time)
            entry.end(end_time=end_time)
            start_time = end_time
            db_session.add(entry)

    db_session.commit()

    yield db_session
