-- CATEGORIES
-- INSERT INTO `CATEGORY` (NAME,CREATED,MODIFIED) VALUES ('start of day','2017-02-15 18:10:00','2017-02-15 18:10:00');
-- INSERT INTO `CATEGORY` (NAME,CREATED,MODIFIED) VALUES ('maintenance & support','2017-02-15 18:10:00','2017-02-15 18:10:00');
-- INSERT INTO `CATEGORY` (NAME,CREATED,MODIFIED) VALUES ('project','2017-02-15 18:10:00','2017-02-15 18:10:00');
-- INSERT INTO `CATEGORY` (NAME,CREATED,MODIFIED) VALUES ('meeting','2017-02-15 18:10:00','2017-02-15 18:10:00');
-- INSERT INTO `CATEGORY` (NAME,CREATED,MODIFIED) VALUES ('break','2017-02-15 18:10:00','2017-02-15 18:10:00');
-- INSERT INTO `CATEGORY` (NAME,CREATED,MODIFIED) VALUES ('end of day','2017-02-15 18:10:00','2017-02-15 18:10:00');


INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',1,'EDI Support Issue',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',2,'EDI Status Meeting',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',3,'Canada Applications Meeting',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',4,'One On One',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',5,'start',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',6,'stop',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',7,'emails',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',8,'general',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
INSERT INTO `INCIDENT` (TYPE,NUMBER,NAME,ACTIVE,PERSISTANT,CREATED,MODIFIED) VALUES ('ACT',9,'lunch',1,1,'2017-02-15 18:10:00','2017-02-15 18:10:00');
