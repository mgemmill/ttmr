import re
from pathlib import Path
import pyperclip


def save_sample():
    name = input("Sample Source: ")
    filepath = Path(name + ".txt")

    with filepath.open(mode="w") as fh_:
        fh_.write(content)


def compile(_regex):
    return re.compile(_regex, re.I | re.M)


remedy_regex = "^(?P<type>(INC|WO))0+(?P<id>[1-9]+\d*)[\r\n]+Mark Gemmill[\r\n]+^(?P<name>[^\r\n]*)[\r\n]?$"
outlook_regex = "(?P<type>(INC|WO))0+(?P<id>[1-9]+\d*).*Description: (?P<name>.*)?$"
er_regex = (
    "(?P<type>FY\d\d)-0+(?P<id>[1-9]+\d*)[^\r\n]*[\r\n]+^(?P<name>[^\r\n]*)[\r\n]+$"
)
jira_regex = (
    "(?P<type>(INC|WO|FY\d\d))[^\d]*0*(?P<id>[1-9]+\d*) *(?P<name>[^\r\n]*)[\r\n]+$"
)


capture_list = [
    compile(remedy_regex),
    compile(outlook_regex),
    compile(er_regex),
    compile(jira_regex),
]


def parse_from(regex, pasted):
    for result in regex.finditer(pasted):
        print(result.groupdict())


def parse_pasted():
    content = pyperclip.paste()
    print(content)
    for rx in capture_list:
        parse_from(rx, content)


print(">" * 60)
parse_pasted()
