import os
import sys

__version__ = '0.4.1'


requirements = ['appdirs',
                'arrow',
                'colorama',
                'docopt',
                'prompt_toolkit',
                'tabulate']

test_requirements = [
    # TODO: put package test requirements here
]


def fetch_sql_includes():
    include_files = []
    for filename in os.listdir('ttmr/sql'):
        include_files.append((os.path.join('ttmr', 'sql', filename),
                              os.path.join('sql', filename)))
    return include_files


setup_options = {
    'name': 'ttmr',
    'version': __version__,
    'description': "Task Timer",
    'author': "Mark Gemmill",
    'author_email': 'mark@markgemmill.com',
    'packages': ['ttmr'],
    'package_dir': {'ttmr': 'ttmr'},
    'include_package_data': True,
    'install_requires': requirements,
    'zip_safe': False,
    'keywords': 'ttmr'}


if 'build_exe' in sys.argv:
    print 'building exe....'
    # cx_freeze options
    from cx_Freeze import Executable
    from cx_Freeze import setup

    build_exe_options = {
        'packages': ['os',
                     'appdirs',
                     'arrow',
                     'dateutil',
                     'colorama',
                     'docopt',
                     'prompt_toolkit',
                     'tabulate'],
        'excludes': ['tkinter', 'unittest'],
        'include_files': fetch_sql_includes(),
        'icon': 'cmd.ico'
    }

    exe = Executable('ttmr_exe.py',
                     targetName='ttmr.exe',
                     base=None)

    setup_options['options'] = {'build_exe': build_exe_options}
    setup_options['executables'] = [exe]

    build_exe_dir = r'build\exe.win32-2.7'
    if os.path.exists(build_exe_dir):
        for file_name in os.listdir(build_exe_dir):
            os.remove(os.path.join(build_exe_dir, file_name))

else:
    print 'normal set up...'
    try:
        from setuptools import setup
    except ImportError:
        from distutils.core import setup
    setup_options['entry_points'] = {
        'console_scripts': ['ttmr = ttmr.cli:main']
    }


setup(**setup_options)
