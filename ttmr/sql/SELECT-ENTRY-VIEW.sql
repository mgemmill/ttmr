SELECT
  E.*
FROM VIEW_ENTRY AS E
WHERE
  STRFTIME('%Y-%m-%d', E.START_TIME) = :day
ORDER BY E.START_TIME ASC
