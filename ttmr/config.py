import os
from ConfigParser import SafeConfigParser
from appdirs import AppDirs
from ttmr.util import DT

DEFAULT_CONFIG = '''
[ttmr]
project_dir=
timezone=US/Pacific
'''


def init_file(path):
    if not os.path.exists(path):
        with open(path, 'w') as fh_:
            fh_.write('')


def assert_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def get_appdir():
    appdir = AppDirs('ttmr', 'mgemmill').user_data_dir
    assert_dir(appdir)
    return appdir


class Config(object):

    def __str__(self):
        output = []
        for k, v in self.__dict__.items():
            if not k.startswith('_'):
                output.append('{}={}'.format(k, v))
        return '\n'.join(output)

    def load_cli_args(self, args):
        for key, val in args.items():
            normalize_key = key.strip('-').replace('-', '_').lower()
            setattr(self, 'cli_' + normalize_key, val)


def config():

    c = Config()

    c.appdir = appdir = get_appdir()
    c.configfile = os.path.join(appdir, 'ttmr.ini')
    c.sqlite = os.path.join(appdir, 'ttmr.db3')

    if not os.path.exists(c.sqlite):
        from db import Database
        with Database(c.sqlite) as db:
            db.initialize()

    if not os.path.exists(c.configfile):
        with open(c.configfile) as fh_:
            fh_.write(DEFAULT_CONFIG)

    parser = SafeConfigParser()
    parser.read(c.configfile)

    c.project_dir = parser.get('ttmr', 'project_dir')
    c.timezone = parser.get('ttmr', 'timezone')

    c.dt = DT(c.timezone)

    return c
