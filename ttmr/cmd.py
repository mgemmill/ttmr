from ttmr.console import Console, GREEN, RED, BLACK, BG_WHITE
from ttmr.db import Database, load_incidences, regex_factory
from ttmr.ui import (option_input, text_input, duration_input, date_input,
                     show_timer, number_input, default_text_input,
                     bool_input)
from ttmr.util import add_table_border


def display_config(conf):
    Console.write(str(conf))


def load(conf):
    load_incidences(conf)


def new_incident(conf):
    subject_text = text_input(u'Email Subject')

    parse_incident_id = regex_factory((r'^.*(?P<type>WO|INC|CRQ|FY\d\d)'
                                       r'0*(?P<number>[0-9]\d+) '
                                       r'.*Description: '
                                       r'(?P<description>.+)'
                                       r'(\d+ [KMG]B|\d+:\d\d [AP]M).*$'))
    incident_id = parse_incident_id(subject_text) or {}

    Console.newline()
    Console.newline()

    inc_type = default_text_input(u'Type', incident_id.get('type', u'WO'))
    inc_num = number_input(u'Number', incident_id.get('number', u''))
    inc_desc = default_text_input(u'Desc', incident_id.get('description', u''))

    Console.newline()

    with Database(conf.sqlite, conf.dt) as db:
        row_update = db.record_incident(inc_type, inc_num, inc_desc)

    if row_update != 1:
        Console.write(RED)
        Console.writeline('Update error. Incicent was not saved.')
        Console.clear_formating()
        return

    Console.write(GREEN)
    Console.write('New incident saved!')
    Console.clear_formating()
    Console.newline()


def time_entry(conf):
    '''Creates a new entry. When doing so it ends the current entry
    (if there is one) and records its duration.

    When the new entry is created, the entries timer is displayed.

    '''
    # fetch working data
    with Database(conf.sqlite, conf.dt) as db:

        categories = db.fetch_category()
        incidents = db.fetch_incidents()
        last_entry = db.fetch_last_entry()

    # get inputs
    category = option_input(u'Category', categories)
    incident = option_input(u'Incident', incidents)

    notes = text_input(u'Notes')
    timestamp = date_input(u'Date', conf.dt)
    duration = duration_input(u'Duration')

    # calculate duration of last entry
    last_entry_id, last_entry_dur = conf.dt.get_duration(last_entry, timestamp)

    with Database(conf.sqlite, conf.dt) as db:
        db.record_entry(category, incident, notes, timestamp, duration)
        if last_entry_id:
            db.update_entry_duration(last_entry_id, last_entry_dur)

    show_timer(timestamp, conf.dt)


def show_current_entry(conf):
    '''Show the last or current entry and display it's timer.
    If there is no open entries, it shows nothing.

    '''
    with Database(conf.sqlite, conf.dt) as db:
        last_entry = db.fetch_last_entry()

    if not last_entry or last_entry.minutes != 0:
        Console.write(RED)
        Console.writeline(' There are no active entries!')
        Console.clear_formating()
        return

    Console.write(str(last_entry))
    Console.newline()

    start_time = conf.dt.parse_date_input(last_entry.start_time)
    show_timer(start_time, conf.dt)


def edit_entry(conf):
    if not conf.cli_curr_obj:
        print "We don't have the selection feature built yet!"
        return

    with Database(conf.sqlite, conf.dt) as db:
        last_entry = db.fetch_last_entry()
        categories = db.fetch_category()
        incidents = db.fetch_incidents()

    # get inputs
    entry_id = last_entry.entry_id
    category = option_input(u'Category', 
                            categories,
                            default=last_entry.category)
    incident = option_input(u'Incident', 
                            incidents, 
                            default=last_entry.incident)

    notes = text_input(u'Notes', default=last_entry.note)
    timestamp = date_input(u'Date', conf.dt, default=last_entry.start_time)

    do_update = bool_input('Proceed with update?')
    
    if not do_update:
        print 'canceling update'
        return

    with Database(conf.sqlite, conf.dt) as db:
        db.update_entry(entry_id, category, incident, notes, timestamp)


def stop_entry(conf):
    '''Ends the current entry (if there is one) and records its
    duration.

    '''
    # fetch working data
    with Database(conf.sqlite, conf.dt) as db:
        last_entry = db.fetch_last_entry()

        if not last_entry:
            Console.write(RED)
            Console.writeline(' There are no entries!')
            return

        Console.writeline(str(last_entry))

        if last_entry.minutes > 0:
            Console.write(RED)
            Console.writeline(' Last entry already has a duration.')
            Console.clear_formating()
            return

        timestamp = date_input(u'End Date', conf.dt)
        duration = duration_input(u'Duration')

        # calculate duration of last entry
        last_entry_id, last_entry_dur = conf.dt.get_duration(last_entry,
                                                             timestamp)

        # if we have a duration, then we override the calculated value
        if last_entry_id and duration:
            last_entry_dur = duration

        if last_entry_id:
            db.update_entry_duration(last_entry_id, last_entry_dur)


def summary(conf):
    '''Displays a summary of the current days total time.

    '''
    from tabulate import tabulate

    with Database(conf.sqlite, conf.dt) as db:
        row = db.fetch_todays_entry_summary()

    Console.write(BG_WHITE)
    Console.write(BLACK)
    Console.newline()

    table = tabulate([row], headers=('DATE',
                                     'START',
                                     'END',
                                     'DURATION'), numalign='decimal')
    Console.write(add_table_border(table))
    Console.newline()
    Console.clear_formating()


def weekly_summary(conf):
    '''Displays a summary of times for a given range of days.

    '''
    from tabulate import tabulate

    start_date = date_input(u'Start Date', conf.dt)
    end_date = date_input(u'End Date', conf.dt)

    with Database(conf.sqlite, conf.dt) as db:
        rows = db.fetch_weekly_totals(start_date.format('YYYY-MM-DD'),
                                      end_date.format('YYYY-MM-DD'))

    Console.write(BG_WHITE)
    Console.write(BLACK)

    table = tabulate(rows,
                     numalign='decimal',
                     headers=('INCIDENT', 'TOTALS', 'SUN', 'MON',
                              'TUE', 'WED', 'THU', 'FRI', 'SAT'))

    Console.write(add_table_border(table))
    Console.newline()
    Console.clear_formating()


def view(conf):
    from tabulate import tabulate
    today = conf.dt.current_timestamp().format('YYYY-MM-DD')
    with Database(conf.sqlite, conf.dt) as db:
        rows = db.fetch_entry_view(today)

    Console.write(BG_WHITE)
    Console.write(BLACK)
    table = tabulate(rows, headers=('TIMESTAMP',
                                    'MIN', 'CATEGORY',
                                    'INCIDENT',
                                    'NOTES'), numalign='decimal')
    Console.write(add_table_border(table))
    Console.newline()
    Console.clear_formating()
