import os
import re
import codecs
import sqlite3
from sqlite3 import IntegrityError
from ttmr.domain import ViewEntry
from ttmr.util import (DB_TIMESTAMP,
                       replace_bad_char,
                       find_data_file)


codecs.register_error('replace', replace_bad_char)


def fetch_sql(sqlfile):
    with open(find_data_file(sqlfile), 'r') as fh_:
        return fh_.read()


def fmt_inc(typ, number):
    return '{}{:0>8}'.format(typ, number)


class Database(object):

    def __init__(self, dbfile, dt):
        self.dbfile = dbfile
        self.dt = dt

    def __enter__(self):
        self.conn = sqlite3.connect(self.dbfile)
        self.csr = self.conn.cursor()
        return self

    @property
    def version(self):
        return sqlite3.sqlite_version

    def initialize(self):
        sql = fetch_sql('INITIALIZE.sql')
        self.csr.executescript(sql)

    def select(self, sql, parms=None):
        try:
            if parms:
                self.csr.execute(sql, parms)
            else:
                self.csr.execute(sql)
            return self.csr.fetchall()
        except IntegrityError as ex:
            raise ex

    def fetch_last_record(self, table):
        sql = 'SELECT * FROM {} ORDER BY ID DESC LIMIT 1;'
        self.csr.execute(sql.format(table))
        return self.csr.fetchone()

    def execute(self, sql, parms, raise_errors=False):
        try:
            self.csr.execute(sql, parms)
        except IntegrityError as ex:
            print parms['number'], ex
            if raise_errors:
                raise ex

    def commit(self):
        self.conn.commit()

    def __exit__(self, *args):
        self.conn.close()

    def record_category(self, *data):
        self.execute(self.new_category_sql, data)
        self.commit()

    def fetch_category(self):
        return self.select(u'SELECT ID, NAME FROM CATEGORY;')

    def record_incident(self, *data):
        # input data
        typ, num, name = data
        timestamp = self.dt.current_timestamp().format(DB_TIMESTAMP)
        parms = {'typ': typ.decode('utf8', 'replace'),
                 'number': num,
                 'name': name.decode('utf8', 'replace'),
                 'active': 1,
                 'persist': 0,
                 'created': timestamp,
                 'modified': timestamp}

        self.execute(fetch_sql('INSERT-INCIDENT.sql'), parms)
        self.commit()
        return self.csr.rowcount

    def fetch_incidents(self):
        return self.select(u'SELECT ID, NAME FROM VIEW_INCIDENTS;')

    def deactivate_incident(self, inc_id):
        parms = {'id': inc_id}
        self.execute(('UPDATE INCIDENT SET ACTIVE = 0 '
                      'WHERE ID = :id AND PERSISTANT <> 1;'), parms)
        self.commit()
        return self.csr.rowcount

    def fetch_last_entry(self):
        sql = u'SELECT * FROM VIEW_ENTRY ORDER BY ENTRY_ID DESC LIMIT 1;'
        self.csr.execute(sql)
        row = self.csr.fetchone()
        if not row:
            return row
        return ViewEntry(*row)

    def record_entry(self, *data):
        category, incident, notes, timestamp, duration = data
        created = self.dt.current_timestamp().format(DB_TIMESTAMP)
        parms = {"category": category,
                 "incident": incident,
                 "note": notes,
                 "timestamp": timestamp.format(DB_TIMESTAMP),
                 "duration": duration,
                 "created": created,
                 "modified": created}
        self.execute(fetch_sql('INSERT-ENTRY.sql'), parms, raise_errors=True)
        self.commit()

    def update_entry(self, *data):
        _id, category, incident, notes, timestamp = data
        modified = self.dt.current_timestamp().format(DB_TIMESTAMP)
        parms = {"id": _id,
                 "category": category,
                 "incident": incident,
                 "note": notes,
                 "timestamp": timestamp.format(DB_TIMESTAMP),
                 "modified": modified}
        self.execute(fetch_sql('UPDATE-ENTRY.sql'), parms, raise_errors=True)
        self.commit()

    def fetch_todays_entry_summary(self):
        sql = u'SELECT * FROM VIEW_CURRENT_TIME_SUMMARY'
        self.csr.execute(sql)
        return self.csr.fetchone()

    def fetch_weekly_totals(self, start_date, end_date):
        sql = fetch_sql('SELECT-WEEK-SUMMARY.sql')
        rows = self.select(sql, parms={'startdate': start_date,
                                       'enddate': end_date})
        return rows

    def fetch_entry_view(self, day):
        sql = fetch_sql('SELECT-ENTRY-VIEW.sql')
        rows = self.select(sql, parms={'day': day})
        return [ViewEntry(*r).to_view() for r in rows]

    def update_entry_duration(self, entry_id, duration):
        created = self.dt.current_timestamp().format(DB_TIMESTAMP)
        sql = fetch_sql('UPDATE-ENTRY-DURATION.sql')
        self.execute(sql, {'dur': duration,
                           'id': entry_id,
                           'mod': created}, raise_errors=True)
        self.commit()


def fetch_directory_incidences(conf):
    '''Look for incidences from directory names.

    '''
    reg = re.compile((r'^(?P<type>WO|INC|CRQ)'
                      r'0*(?P<number>[0-9]\d+) '
                      r'?- ?(?P<name>.*)\w?$'), re.I)

    if not os.path.exists(conf.project_dir):
        print 'The project directory does not exist. Nothing to load.'
        return

    for entry in os.listdir(conf.project_dir):
        m = reg.match(entry)
        if m:
            g = m.groupdict()
            yield (g['type'], int(g['number']), g['name'].strip())


def regex_factory(regex):
    '''Convenience wrapper around regex with named capture groups.
    '''
    reg = re.compile(regex, re.I)

    def _match(text):
        m = reg.match(text)
        if m:
            return m.groupdict()

    return _match


def fetch_file_incidences(conf):
    '''Look for incidences from a files content.

    '''
    # get from list
    reg = re.compile((r'^(?P<type>WO|INC|CRQ|FY\d\d)'
                      r'0*(?P<number>[0-9]\d+)$'), re.I)

    remedyfile = os.path.join(conf.project_dir, 'remedy-tickets.txt')

    if not os.path.exists(remedyfile):
        print 'Remedy file does not exist!'
        return

    with open(remedyfile) as fh_:
        lines = fh_.readlines()

    parse_incident_id = regex_factory((r'^(?P<type>WO|INC|CRQ)'
                                       r'0*(?P<number>[0-9]\d+)$'))

    ilines = iter(lines)
    for line in ilines:
        g = parse_incident_id(line)
        if g:
            next(ilines)  # skip a line
            g['name'] = next(ilines).strip()
            yield (g['type'], int(g['number']), g['name'].strip())


def load_incidences(conf):

    proj = set()

    for dir_inc in fetch_directory_incidences(conf):
        proj.add(dir_inc)

    for file_inc in fetch_directory_incidences(conf):
        proj.add(file_inc)

    projects = list(proj)
    projects.sort()

    with Database(conf.sqlite, conf.dt) as db:
        print 'loading....'
        for typ, num, nme in projects:
            fmt = u'{}{:0>8} - {}'
            print fmt.format(typ, num, nme.decode('utf8', 'ignore'))
            db.record_incident(typ, num, nme)

        active_inc = {r[1]: r[0] for r in db.fetch_incidents()}
        inc_keys = active_inc.keys()

        def match_key(inc):
            for key in inc_keys:
                if inc in key:
                    return key

        for typ, num, nme in projects:
            m = match_key(fmt_inc(typ, num))
            if m:
                try:
                    active_inc.pop(m)
                except KeyError:
                    pass
        
        # deactivate items
        for key, val in active_inc.items():
            cnt = db.deactivate_incident(val)
            if cnt > 1:
                print 'X', cnt, key

    return projects
